<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2015 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_'))
	exit;

include_once dirname(__FILE__).'/../../classes/feedback.php';

class AdminFeedbackController extends ModuleAdminController
{

	public function __construct()
	{
		$this->table = 'feedback';
		$this->className = 'Feedback';
		$this->lang = true;
		$this->bootstrap = true;

		parent::__construct();

		// HelperList Fields params
		$this->fields_list = array(
			'id_feedback' => array('title' => '#'),
			'customer_name' => array('title' => $this->l('Customer'), 'filter_key' => 'cu!lastname'),
			'feedback' => array('title' => $this->l('Feedback'), 'callback' => 'getCleanFeedback'),
			'active' => array('title' => $this->l('Active'), 'active' => 'status')
		);

		// Join and Select to add Customer Name
		$this->_join .= 'LEFT JOIN `'._DB_PREFIX_.'customer` cu ON a.`id_customer` = cu.`id_customer`';
		$this->_select .= 'IF(a.`id_customer`>0,CONCAT( cu.firstname, \' \', cu.lastname),\''.$this->l('Guest').'\') as customer_name';

		if (Shop::isFeatureActive())
		{
			// we add restriction for shop
			$id_shop_list = Shop::getContextListShopID();
			$this->_where = ' AND b.`id_shop` IN ('.implode(', ', $id_shop_list).')';

			// we add shop fields
			$this->_select .= ', s.name as shop_name';
			$this->_join .= 'INNER JOIN '._DB_PREFIX_.'shop s ON (s.id_shop = b.id_shop) ';
			$this->fields_list['shop_name'] = array(
				'title' => $this->l('Shop'),
				'filter_key' => 's!name'
			);
		}

		// HelperForm Fields params
		$this->fields_form = array(
			'legend' => array(
				'title' => $this->l('Edit Feedback')
			),
			'input' => array(
				array(
					'type' => 'switch',
					'label' => $this->l('Published:'),
					'name' => 'active',
					'required' => false,
					'class' => 't',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Enabled')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('Disabled')
						)
					)
				),
				array(
					'type' => 'textarea',
					'label' => $this->l('Feedback:'),
					'name' => 'feedback',
					'lang' => true,
				),
				array(
					'type' => 'select',
					'name' => 'id_customer',
					'label' => $this->l('Customer'),
					'options' => array(
						'query' => Customer::getCustomers(),
						'id' => 'id_customer',
						'name' => 'lastname',
						'default' => array(
							'label' => $this->l('Guest'),
							'value' => 0
						)
					)
				)
			),
			'submit' => array(
				'title' => $this->l('Save'),
			)
		);

		$this->bulk_actions = array(
			'delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')),
			'enableSelection' => array('text' => $this->l('Enable selection')),
			'disableSelection' => array('text' => $this->l('Disable selection'))
		);

		$this->addRowAction('edit');
		$this->addRowAction('delete');
	}

	/**
	 * We need a shop to add New Feedback
	 * Remove new button if no shop selected
	 */
	public function initToolbar()
	{
		parent::initToolbar();
		if (Shop::isFeatureActive() && Shop::getContext() !== Shop::CONTEXT_SHOP)
			if (isset($this->toolbar_btn['new']))
			{
				$this->displayWarning($this->l('You must select a shop to add feedback.'));
				unset($this->toolbar_btn['new']);
			}
	}

	/**
	 * We need a shop to add New Feedback.
	 * Show error if none selected
	 */
	public function initContent()
	{
		if (Shop::isFeatureActive() && Shop::getContext() !== Shop::CONTEXT_SHOP && $this->display == 'add')
			$this->template = 'selectshop.tpl';
		parent::initContent();
	}

	/**
	 * Security : Never trust foreign data
	 * @param string feedback
	 * @return string
	 */
	public static function getCleanFeedback($feedback)
	{
		return Tools::substr(Tools::safeOutput($feedback), 0, 80);
	}

	/**
	 * Install AdminFeedback in customer back office menu
	 * @return boolean true if success
	 */
	public static function installInBO()
	{
		$new_menu = new Tab();
		$new_menu->id_parent = Tab::getIdFromClassName('AdminParentCustomer');
		$new_menu->class_name = 'AdminFeedback';
		$new_menu->module = 'customerfeedback';
		$new_menu->active = 1;

		// Set menu name in all active Language.
		$languages = Language::getLanguages(true);
		foreach ($languages as $language)
			$new_menu->name[(int)$language['id_lang']] = 'Manage feedback';

		return $new_menu->save();
	}

	/**
	 * Remove AdminFeedback in customer back office menu
	 * @return boolean true if success
	 */
	public static function removeInBO()
	{
		$remove_id = Tab::getIdFromClassName('AdminFeedback');
		if ($remove_id)
		{
			$to_remove = new Tab($remove_id);
			if (validate::isLoadedObject($to_remove))
				return $to_remove->delete();
		}
		return false;
	}

}
