<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2015 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_'))
	exit;

include_once dirname(__FILE__).'/../../classes/feedback.php';

class CustomerfeedbackGetFeedbackModuleFrontController extends ModuleFrontController
{

	/**
	 * postProcess handles submit
	 */
	public function postProcess()
	{
		parent::postProcess();

		if (Tools::isSubmit('feedback'))
		{
			$customer_feedback = Tools::getValue('feedback', false);

			if (!$customer_feedback)
				$this->errors[] = Tools::displayError($this->module->l('The feedback cannot be blank.'));
			elseif (!Validate::isString($customer_feedback))
				$this->errors[] = Tools::displayError($this->module->l('Invalid feedback.'));

			// If no error, save feedback
			if (!count($this->errors))
			{
				$feedback = new Feedback();
				$feedback->id_customer = $this->context->customer->id;
				$feedback->active = false;
				$feedback->feedback[$this->context->language->id] = $customer_feedback;
				$feedback->add();
				Tools::redirect($this->context->link->getModuleLink('customerfeedback', 'conffeedback'));
				exit;
			}
		}
	}

	/**
	 * Add Front controller CSS
	 */
	public function setMedia()
	{
		parent::setMedia();
		$this->context->controller->addCSS(__PS_BASE_URI__.'modules/feedback/views/css/getfeedback.css', 'all');
	}

	/**
	 * Assign smarty template
	 */
	public function initContent()
	{
		parent::initContent();

		$this->context->smarty->assign('errors', $this->errors);
		$this->setTemplate('getfeedback.tpl');
	}

}
