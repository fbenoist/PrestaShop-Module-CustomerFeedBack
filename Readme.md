# Customer Feedback

PrestaShop Sample Module

DON'T USE ON PRODUCTION SERVER

## Licensing

Licensed under the [Open Software License (OSL) v3.0](http://www.prestashop.com/en/osl-license).

## About Prestashop

[Prestashop](http://www.prestashop.com) is an ecommerce application licensed under the OSL v3.0 license. ©PrestaShop, Inc.

## About Frédéric BENOIST

Prestashop Expert
Official trainer for Developers and Integrators
http://www.fbenoist.com