{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if isset($feedbacks)}
	<div class="clearfix"></div>
	<section id="block_feedback" class="footer-block col-xs-12">
	<h4 class="title_block">{l s='Customer feedback' mod='customerfeedback'}</h4>
	<ul class="toggle-footer">
		{foreach from=$feedbacks item=feedback}
			<li class="item">
				{$feedback.feedback|escape:'htmlall':'UTF-8'}
			</li>
		{/foreach}
        </ul>
	</section>
{/if}
<section id="block_feedback_link" class="bottom-footer col-xs-12">
        <a href="{$link->getModuleLink('customerfeedback','getfeedback')|escape:'html':'UTF-8'}" class="lnk_more pull-right">{l s='Give us some feedback' mod='customerfeedback'}</a>
</section>
