{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{capture name=path}{l s='Give use some feedback' mod='customerfeedback'}{/capture}
<h1>{l s='Customer feedback' mod='customerfeedback'}</h1>
<p>{l s='Here\'s your chance to give us some feedback.' mod='customerfeedback'}</p>

{include file="$tpl_dir./errors.tpl"}


<div id="getfeedback">
    <form method="post" class="std" name="feedback">
	<textarea class="form-control" cols="120" rows="10" name="feedback" id="feedback"></textarea>
	<br/>
	<ul class="footer_links clearfix">
		<li>
			<button class=" btn btn-default button-medium" href="{$base_dir|escape:'html':'UTF-8'}">
				<span><i class="icon-chevron-left"></i> {l s='Home' mod='customerfeedback'}</span>
			</button>
		</li>
		<li class="pull-right">
			<button type="submit" name="processFeedback" class="btn btn-primary button-medium">
				<span>{l s='Send' mod='customerfeedback'} <i class="icon-chevron-right right"></i></span>
			</button>
		</li>

	</ul>
    </form>

</div>
