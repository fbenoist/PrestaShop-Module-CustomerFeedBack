<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2015 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_'))
	exit;

class Feedback extends ObjectModel
{

	/** @var integer Customer Feedback ID */
	public $id_feedback;

	/** @var integer Customer ID */
	public $id_customer;

	/** @var string Object creation date */
	public $date_add;

	/** @var string Object last modification date */
	public $date_upd;

	/** @var boolean Status */
	public $active = false;

	/** @var integer Shop ID */
	public $id_shop;

	/** @var string Feedback */
	public $feedback;

	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'feedback',
		'primary' => 'id_feedback',
		'multilang' => true,
		'multilang_shop' => true,
		'fields' => array(
			'id_feedback' => array('type' => Self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'id_customer' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
			'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
			'active' => array('type' => self::TYPE_BOOL),
			'id_shop' => array('type' => self::TYPE_INT, 'lang' => true, 'validate' => 'isUnsignedInt'),
			'feedback' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
		)
	);

	/**
	 * Create Table in Db
	 * @return boolen true if success
	 */
	public static function createDBTable()
	{
		return Db::getInstance()->Execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'feedback` (
                    `id_feedback` INT(10) NOT NULL AUTO_INCREMENT,
                    `id_customer` INT(10) UNSIGNED NOT NULL,
                    `date_add` DATETIME NOT NULL ,
                    `date_upd` DATETIME NOT NULL ,
                    `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT \'0\',
                    PRIMARY KEY(`id_feedback`),
                    KEY `id_customer` (`id_customer`)
                ) ENGINE = '._MYSQL_ENGINE_.' CHARACTER SET utf8 COLLATE utf8_general_ci') && Db::getInstance()->Execute('
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'feedback_lang` (
                        `id_feedback` INT(10) UNSIGNED NOT NULL,
                        `id_shop` int(11) unsigned NOT NULL DEFAULT \'1\',
                        `id_lang` int(10) unsigned NOT NULL,
                        `feedback` TEXT,
                         PRIMARY KEY (`id_feedback`,`id_shop`,`id_lang`),
                         KEY `id_lang` (`id_lang`)
                ) ENGINE = '._MYSQL_ENGINE_.' CHARACTER SET utf8 COLLATE utf8_general_ci');
	}

	/**
	 * Update Feedback
	 * ObjectModele->update read shop_list in Current Context.
	 * Whe need to read it from current record
	 *
	 * @param boolean $null_values
	 * @return boolean update result
	 */
	public function update($null_values = false)
	{
		$this->id_shop_list = array($this->id_shop);
		return parent::update($null_values);
	}

	/**
	 * Read published Customer feedback
	 * @param int $id_lang
	 * @param int $id_shop
	 * @return array of customer feedback records.
	 */
	public static function getPublished($id_lang = null, $id_shop = null)
	{
		$id_lang = is_null($id_lang) ? Context::getContext()->language->id : (int)$id_lang;
		$id_shop = is_null($id_shop) ? Context::getContext()->shop->id : (int)$id_shop;

		// Try to read all customer feedback records in cache
		$cache_id = 'CustomerFeedback_All_'.(int)$id_shop.'_'.(int)$id_lang;
		if (!Cache::isStored($cache_id))
		{
			$sql = new DbQuery();
			$sql->from('feedback', 'f');
			$sql->leftJoin('feedback_lang', 'fl', 'f.id_feedback = fl.id_feedback AND fl.id_lang = '.(int)$id_lang);
			if (Shop::isFeatureActive())
				$sql->where('fl.`id_shop` = '.(int)$id_shop);
			$sql->where('f.active = 1');

			// Use _PS_USE_SQL_SLAVE_, to select slaves database in a replication environment
			if ($result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql))
				Cache::store($cache_id, $result);
		}
		else
			$result = Cache::retrieve($cache_id);

		return $result;
	}

	/**
	 * Override clearCache to remove new cache entry
	 * @param boolean $all
	 */
	public function clearCache($all = false)
	{
		parent::clearCache($all);
		Cache::clean('CustomerFeedback_All_*');
	}

}
