<?php
/**
 * 2007-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2015 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_'))
	exit;

include_once dirname(__FILE__).'/classes/feedback.php';
include_once dirname(__FILE__).'/controllers/admin/adminfeedbackController.php';

class CustomerFeedback extends Module
{

	public function __construct()
	{
		$this->name = 'customerfeedback';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Frédéric BENOIST';
		$this->need_instance = false;
		parent::__construct();
		$this->displayName = $this->l('Customer feedback');
		$this->description = $this->l('Customer feedback management.');
	}

	public function install()
	{
		if (Shop::isFeatureActive())
			Shop::setContext(Shop::CONTEXT_ALL);

		if (!parent::install()
			|| !$this->registerHook('displayFooter')
			|| !$this->registerHook('header')
			|| !$this->registerHook('actionObjectFeedbackDeleteAfter')
			|| !$this->registerHook('actionObjectFeedbackAddAfter')
			|| !$this->registerHook('actionObjectFeedbackUpdateAfter')
			|| !Feedback::createDBTable()
			|| !AdminFeedbackController::installInBO())
			return false;

		return true;
	}

	public function uninstall()
	{
		return parent::uninstall()
			&& AdminFeedbackController::removeInBO()
			&& Db::getInstance()->Execute('DROP TABLE `'._DB_PREFIX_.'feedback`')
			&& Db::getInstance()->Execute('DROP TABLE `'._DB_PREFIX_.'feedback_lang`');
	}

	public function hookDisplayFooter()
	{
		if (!$this->isCached('footer.tpl', $this->getCacheId()))
		{
			if ($feedbacks = Feedback::getPublished())
				$this->context->smarty->assign('feedbacks', $feedbacks);
		}

		return $this->display(__FILE__, 'footer.tpl', $this->getCacheId());
	}

	public function hookHeader()
	{
		$this->context->controller->addCSS($this->_path.'views/css/footer.css', 'all');
	}

	/**
	 * Clear Smarty cache after Delete
	 * @param type $params
	 */
	public function hookactionObjectFeedbackDeleteAfter()
	{
		$this->_clearCache('feedback.tpl');
	}

	/**
	 * Clear Smarty cache after Insert
	 * @param type $params
	 */
	public function hookactionObjectFeedbackAddAfter()
	{
		$this->_clearCache('feedback.tpl');
	}

	/**
	 * Clear Smarty cache after Update
	 * @param type $params
	 */
	public function hookactionObjectFeedbackUpdateAfter()
	{
		$this->_clearCache('feedback.tpl');
	}

}
